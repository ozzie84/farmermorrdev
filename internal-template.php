<?php

/* Template Name: Internal template */

// Internal Content

remove_action('genesis_loop', 'genesis_do_loop', 10);
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

add_action('genesis_loop', 'custom_inner_content', 10);
remove_action('genesis_before_content_sidebar_wrap', 'custom_do_breadcrumbs', 5);


function custom_inner_content() {?>

    <div class="main-content">
        <div class="container custom-container">
            <div class="row">
                <div class="col-12">
                    <?php
                    global $post;
                    echo apply_filters( 'the_content', $post->post_content );
                    ?>
                </div>
            </div>
        </div>
    </div>

<?php }

genesis();