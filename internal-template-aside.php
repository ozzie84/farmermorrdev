<?php

/* Template Name: Internal template aside */
// Internal Content

remove_action('genesis_loop', 'genesis_do_loop', 10);
add_filter('genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
add_action('genesis_loop', 'custom_inner_content', 10);
remove_action('genesis_before_content_sidebar_wrap', 'custom_do_breadcrumbs', 5);

function custom_inner_content() {?>

    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-12">
                    <?php
                     global $post;
                     if (has_post_thumbnail()) {
                        echo the_post_thumbnail();
                     }
                     echo apply_filters('the_content', $post->post_content);
                    ?>
                </div>
                <div class="col-lg-4 col-md-12 col-12">
                    <aside class="main-content__sidebar">
                        <?php genesis_do_sidebar();?>
                    </aside>
                </div>
            </div>
        </div>
        <div class="container mt-4 mb-4">
            <div class="section section-4 awards-section">
                <div class="title-global--wrap mb3" style="background-color: hsl(37, 56%, 62%);">
                    <div class="title-global fs25">
                        <h2 class="title-font" style="background-color: hsl(37, 56%, 62%); color: #2d1e03;">Affiliations and Accolades</h2>
                    </div>
                </div>
                <div class="awards--wrap">
                    <ul class="row">
                        <?php
                        if( have_rows('awards') ):
                            while( have_rows('awards') ) : the_row();
                                ?>
                                <li class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                    <div class="award-inner">
                                        <img src="<?php echo get_sub_field('image');?>" alt="award" class="award-inner--img">
                                    </div>
                                </li>
                            <?php
                            endwhile;
                        endif; ?>
                    </ul>
                </div>
            </div>
            <div class="section section-5 attorney-practice-area" style="background-color: hsl(37, 56%, 62%);">
                <div class="title-global--wrap" style="background-color: hsl(37, 56%, 62%);">
                    <div class="title-global fs25">
                        <h2 class="title-font" id="attorneys-name" style="background-color: hsl(37, 56%, 62%); color: #2d1e03;">Practice Areas</h2>
                    </div>
                </div>
                <div class="row">
                    <?php
                    if( have_rows('practice') ):
                        while( have_rows('practice') ) : the_row();
                            ?>
                            <div class="<?php echo "col-lg-" . get_sub_field('col') . " col-md-6 col-sm-6 col-xs-12" ?>">
                                <h2 class="practice-area-heading"><i class="fa fa-lightbulb-o" aria-hidden="true"></i><?php echo get_sub_field('name');?></h2>
                                <a class="practice-area-link img-link" title="<?php echo get_sub_field('name');?>" href="<?php the_permalink(get_sub_field('pageid')) ;?>""><img class="practice-area-img" src="<?php echo get_sub_field('image');?>" alt="image" /></a>
                            </div>
                        <?php
                        endwhile;
                    else :
                    endif; ?>
                </div>
            </div>
            <div class="section section-6 integrity-section" style="background-color: hsl(39, 87%, 9%);">
                <div class="title-global--wrap" style="background-color: hsl(37, 56%, 62%)">
                    <div class="title-global fs25">
                        <h2 class="title-font" style="background-color: hsl(37, 56%, 62%); color: #fff;">Meet Our Other Attorneys</h2>
                    </div>
                </div>
            </div>
            <div class="section section-7 other-attorney">
                <div class="person--wrap">
                    <ul class="person">
                        <?php
                        if( have_rows('attorneys') ):
                            while( have_rows('attorneys') ) : the_row();
                                ?>
                                <li class="person__item">
                                    <a href="<?php the_permalink(get_sub_field('pageid')) ;?>" class="person__link" title="<?php echo get_sub_field('name');?>">
                                        <img src="<?php echo get_sub_field('image');?>" alt="person">
                                    </a>
                                </li>
                            <?php
                            endwhile;
                        else :
                        endif; ?>
                    </ul>
                </div>
            </div>
            <div class="section section-8 integrity-section">
                <div class="title-global--wrap" style="background-color: hsl(39, 87%, 9%)">
                    <div class="title-global fs25">
                        <h2 class="title-font" style="background-color: hsl(39, 87%, 9%); color: #fff;">Integrity . Experience . Results</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php }

genesis();