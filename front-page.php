<?php

// add homepage hero image
add_action('genesis_after_header', 'custom_front_page_after_header', 10);

function custom_front_page_after_header() {

    ?>

    <div class="homepage-hero-image">
        <div class="hero--wrap" id="hero">
            <div class="hero">
                <div class="hero__line">
                    <a href="<?php the_permalink('2829')?>" class="hero__link">An Important Message from Farmer & Morris Regarding COVID-19 and Office Procedures</a>
                </div>
                <h1 class="hero__title title-font">Farmer & Morris Law, PLLC</h1>
                <div class="hero__main">
                    <div class="hero__banner">
                        <div class="hero__image">
                            <picture>
                                <source srcset="<?=CHILD_URL?>/assets/app/img/law-team-sm.jpg" data-srcset="<?=CHILD_URL?>/assets/app/img/law-team-sm.jpg" media='(max-width: 576px)' type='image/jpg'/>
                                <img src="<?=CHILD_URL?>/assets/app/img/law-team.jpg" alt="Law Team">
                            </picture>
                        </div>
                    </div>
                    <div class="hero__form">
                        <h3 class="hero__form--title">
                            FREE CASE EVALUATION
                            <br>
                            <span>EASY. QUICK. CONFIDENTIAL.</span>
                        </h3>
                        <?php echo do_shortcode('[contact-form-7 id="2878" title="form hero" html_class="form-hero"]')?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
}

remove_action('genesis_loop', 'genesis_do_loop', 10);
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//homepage content
add_action('genesis_loop', 'custom_homepage_content', 10);
remove_action('genesis_before_content_sidebar_wrap', 'custom_do_breadcrumbs', 5);

function custom_homepage_content(){ ?>

    <?php global $hc_settings; ?>

    <div class="title-global--wrap" style="background-color: hsl(39, 87%, 9%)">
        <div class="container">
            <div class="title-global">
                <h2 class="title-font" style="background-color: hsl(39, 87%, 9%)">Integrity. Experience. Results.</h2>
            </div>
        </div>
    </div>

    <div class="about">
        <p class="about__sub">
            Voted Best Attorney of Rutherford County 2011, 2012, 2013, 2014, 2015, 2016, 2017 and 2018 – Mark Morris
        </p>
        <p class="about__text">
            Our goal is to provide excellence in legal representation. Our firm offers a broad range of legal services to meet our clients’ needs. Every client is extremely important to us, and we strive to give the assistance needed to obtain the best results possible.
        </p>
    </div>

    <div class="title-global--wrap" style="background-color: hsl(37, 56%, 62%)">
        <div class="container">
            <div class="title-global">
                <h2 class="title-font" style="background-color: hsl(37, 56%, 62%); color: rgb(45,30,3);">Affiliations and Accolades</h2>
            </div>
        </div>
    </div>

    <div class="awards--wrap">
        <div class="container">
            <div class="awards">
                <div class="awards__four">
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link1.png" alt="award">
                        </div>
                    </div>
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link2.png" alt="award">
                        </div>
                    </div>
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link3.png" alt="award">
                        </div>
                    </div>
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link4.png" alt="award">
                        </div>
                    </div>
                </div>
                <div class="awards__four">
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link5.png" alt="award">
                        </div>
                    </div>
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link6.png" alt="award">
                        </div>
                    </div>
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link7.png" alt="award">
                        </div>
                    </div>
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link8.png" alt="award">
                        </div>
                    </div>
                </div>
                <div class="awards__four">
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link9.png" alt="award">
                        </div>
                    </div>
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link10.png" alt="award">
                        </div>
                    </div>
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link11.png" alt="award">
                        </div>
                    </div>
                    <div class="award">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link12.png" alt="award">
                        </div>
                    </div>
                </div>
                <div class="awards__three">
                    <div class="award">
                        <a href="https://www.scbar.org/" target="_blank" rel="nofollow" class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link13.png" alt="award">
                        </a>
                    </div>
                    <div class="award">
                        <a href="https://www.nacba.org/" target="_blank" rel="nofollow" class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link14.png" alt="award">
                        </a>
                    </div>
                    <div class="award">
                        <a href="https://www.ncaj.com/" target="_blank" rel="nofollow" class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link15.png" alt="award">
                        </a>
                    </div>
                </div>
                <div class="awards__three avvo--wrap">
                    <a href="https://www.avvo.com/attorneys/28139-nc-joshua-farmer-1739509.html?utm_campaign=avvo_rating&utm_content=1217583&utm_medium=avvo_badge&utm_source=avvo" target="_blank" rel="nofollow" class="award avvo">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/badge_avvo_rating.png" alt="award">
                        </div>
                        <div class="award__rating">
                            <div class="rating">
                                <span>9.8</span>
                                <span>Superb</span>
                            </div>
                            <p class="award__info">
                                Top Attorney
                                <br>
                                Litigation
                            </p>
                        </div>
                    </a>
                    <a href="https://www.avvo.com/attorneys/28139-nc-joshua-farmer-1739509.html?utm_campaign=avvo_contributor_badge&utm_content=1739509&utm_medium=avvo_badge&utm_source=avvo" target="_blank" rel="nofollow" class="award avvo">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/badge_reviews.png" alt="award">
                        </div>
                        <div class="award__rating">
                            <div class="top">
                                <span>Top Contributor</span>
                                <span>2014</span>
                            </div>
                            <p class="award__info">
                                Litigation
                            </p>
                        </div>
                    </a>
                    <a href="https://www.avvo.com/attorneys/28139-nc-joshua-farmer-1739509.html?utm_campaign=avvo_review_badge&utm_content=1739509&utm_medium=avvo_badge&utm_source=avvo" target="_blank" rel="nofollow" class="award avvo">
                        <div class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/badge_top_contributor.png" alt="award">
                        </div>
                        <div class="award__rating">
                            <div class="top">
                                <span>Clients’ Choice</span>
                                <span>2016</span>
                            </div>
                            <p class="award__info">
                                Litigation
                            </p>
                        </div>
                    </a>
                </div>
                <div class="awards__three">
                    <div class="award">
                        <a href="https://www.ncbar.org/" target="_blank" rel="nofollow" class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link16.png" alt="award">
                        </a>
                    </div>
                    <div class="award">
                        <a href="http://www.naopia.com/" target="_blank" rel="nofollow" class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link17.png" alt="award">
                        </a>
                    </div>
                    <div class="award">
                        <a href="https://www.nosscr.org/" target="_blank" rel="nofollow" class="award__link">
                            <img src="<?=CHILD_URL?>/assets/dev/img/link18.png" alt="award">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="data--wrap">
        <div class="container">
            <div class="data" id="counterJS">
                <div class="data__item">
                    <h4 class="data__sub" data-count="6">6</h4>
                    <p class="data__name title-font">Attorneys</p>
                </div>
                <div class="data__item">
                     <h4 class="data__sub" data-count="4">4</h4>
                    <p class="data__name title-font">Offices</p>
                </div>
                <div class="data__item">
                     <h4 class="data__sub" data-count="8">8</h4>
                    <p class="data__name title-font">Staff</p>
                </div>
                <div class="data__item">
                     <div class="plus-fix--wrap">
                         <h4 class="data__sub plus-fix" data-count="10000">10000</h4>
                        <span>+</span>
                     </div>
                    <p class="data__name title-font">Clients served</p>
                </div>
                <div class="data__item">
                     <h4 class="data__sub" data-count="1">1</h4>
                    <p class="data__name title-font">goal</p>
                </div>
            </div>
        </div>
    </div>

    <div class="title-global--wrap title-white" style="background-color: white">
        <div class="container">
            <div class="title-global">
                <h2 class="title-font" style="background-color: white; color: rgb(45,30,3);">Practice Areas</h2>
            </div>
        </div>
    </div>

    <div class="layers--wrap">
        <ul class="layers">
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-ambulance"></i>
                    </div>
                    <h4 class="layers__title">
                        Personal Injury
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('785')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project1.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-users"></i>
                    </div>
                    <h4 class="layers__title">
                        Family Law
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('111')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project2.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-university"></i>
                    </div>
                    <h4 class="layers__title">
                        Wrongful Death
                    </h4>
                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('101')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project3.jpg" alt="project">
                    </a>
                </div>
            </li>

            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-money-bill-alt"></i>
                    </div>
                    <h4 class="layers__title">
                        Bankruptcy
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('1997')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project4.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-taxi"></i>
                    </div>
                    <h4 class="layers__title">
                        Speeding Tickets
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('115')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project5.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-gavel"></i>
                    </div>
                    <h4 class="layers__title">
                        Personal Injury
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('125')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project6.jpg" alt="project">
                    </a>
                </div>
            </li>

            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fab fa-stack-overflow"></i>
                    </div>
                    <h4 class="layers__title">
                        Wills and Estates
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('119')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project7.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-balance-scale"></i>
                    </div>
                    <h4 class="layers__title">
                        Civil Litigation
                    </h4>
                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('117')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project8.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-medkit"></i>
                    </div>
                    <h4 class="layers__title">
                        Workers’ Compensation
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('660')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project9.jpg" alt="project">
                    </a>
                </div>
            </li>

            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-ambulance"></i>
                    </div>
                    <h4 class="layers__title">
                        Business Law
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('113')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project10.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-briefcase"></i>
                    </div>
                    <h4 class="layers__title">
                        Construction Law
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('123')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project11.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-wheelchair"></i>
                    </div>
                    <h4 class="layers__title">
                        Social Security Disability
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('105')?>" class="layers__link">
                        <img src="<?=CHILD_URL?>/assets/dev/img/project12.jpg" alt="project">
                    </a>
                </div>
            </li>

        </ul>
    </div>

    <div class="title-global--wrap title-white" style="background-color: white">
        <div class="container">
            <div class="title-global">
                <h2 class="title-font" style="background-color: white; color: rgb(45,30,3);">From Our Clients</h2>
            </div>
        </div>
    </div>

    <div class="reviews--wrap">
        <ul class="reviews">
           <li class="reviews__item">
               <div class="reviews__icon">
                   <i class="fas fa-university"></i>
               </div>
               <div class="reviews__main">
                   <h4 class="reviews__title">
                       “Great Family Law Attorney…”
                   </h4>
                   <p class="reviews__text">
                       At one of the most difficult times in my life, Mark stepped in. He was more than just my divorce attorney, he was a friend. He gracefully led me through each step, assuring me that everything would work out. I am completely satisfied with his services and altogether impressed with he and his staff…He was so humble and caring…I highly recommend Mark, he is a great guy and makes a wonderful family lawyer.
                   </p>
                   <span class="reviews__author">
                       – Amy, a Client
                   </span>
               </div>
           </li>
           <li class="reviews__item">
                <div class="reviews__icon">
                    <i class="fas fa-balance-scale"></i>
                </div>
                <div class="reviews__main">
                    <h4 class="reviews__title">
                        “A Huge Load Relieved…”
                    </h4>
                    <p class="reviews__text">
                        Josh was so easy to talk to and was a great and focused listener. He made me feel confident in the case and totally optimistic. I felt like a huge load was taken off my shoulder and that he was taking on that burden… I really did not worry about the hearing at all just knowing that he was going to do all the work. He gave me so much peace. he won the case for me and I was so comfortable during the entire questioning. I recommend him highly to anyone. You will be very pleased.
                    </p>
                    <span class="reviews__author">
                       – Jayne, a Client
                   </span>
                </div>
            </li>
           <li class="reviews__item">
                <div class="reviews__icon">
                    <i class="fas fa-gavel"></i>
                </div>
                <div class="reviews__main">
                    <h4 class="reviews__title">
                        “Full Confidence in My Attorney…”
                    </h4>
                    <p class="reviews__text">
                        Andrea made me feel very comfortable and let me know from the beginning that she really cares for her clients and their well being. She is very thorough and kept me well informed of what to expect. She was always available if I had questions concerning my case and she did not make me feel like I was bothering her. I have full confidence in Andrea. Thank you Andrea!
                    </p>
                    <span class="reviews__author">
                       – Debbie, a Client
                   </span>
                </div>
            </li>
           <li class="reviews__item">
                <div class="reviews__icon">
                    <i class="fas fa-university"></i>
                </div>
                <div class="reviews__main">
                    <h4 class="reviews__title">
                        “Hope for the Future…”
                    </h4>
                    <p class="reviews__text">
                        Mr. Farmer and his staff did a good job for me. It was refreshing to deal with a lawyer who treated me like I was more important than his fee. Very client focused and honest. Hire with confidence!
                    </p>
                    <span class="reviews__author">
                       – Tony, a Client
                   </span>
                    <p class="reviews__text">
                        Caleb was very professional and worked to understand our situation. He and his staff were very good at answering any questions or correspondence we had. He worked diligently to get our case heading in a direction that gave us hope for the future. I highly recommend him.
                    </p>
                    <span class="reviews__author">
                        – David, a Client
                    </span>
                </div>
            </li>
           <li class="reviews__item">
                <div class="reviews__icon">
                    <i class="fas fa-balance-scale"></i>
                </div>
                <div class="reviews__main">
                    <h4 class="reviews__title">
                        “Excellence at its Best…”
                    </h4>
                    <p class="reviews__text">
                        One phone call, one small fee and the absolute best customer service I have ever experienced. I was assured of the results before I ever paid a dime. My ticket was dismissed and so zero points on my driver’s license. I have never been treated this good. Thank you Mrs. Davies. I would recommend you to any and all I know as the BEST attorney in the business.
                    </p>
                    <span class="reviews__author">
                       – Doug, a Client
                   </span>
                </div>
            </li>
           <li class="reviews__item">
                <div class="reviews__icon">
                    <i class="fas fa-gavel"></i>
                </div>
                <div class="reviews__main">
                    <h4 class="reviews__title">
                        “A Passionate Attorney…”
                    </h4>
                    <p class="reviews__text">
                        She is a wonderful individual. She is passionate, caring, honest, and extremely hard working. She certainly has her clients’ best interests at heart and works hard to see the best result for every client. Mrs. Valentine is a persuasive trial advocate.
                    </p>
                    <span class="reviews__author">
                       – Blake, a Defense Attorney
                   </span>
                </div>
            </li>
        </ul>
    </div>

    <div class="recall--wrap" style="background-image: url(<?=CHILD_URL?>/assets/dev/img/bg.jpg)">
        <div class="container">
            <div class="recall">
                <a href="tel:+1<?=$hc_settings['phone_number']?>" class="recall__link" data-num="<?=$hc_settings['phone_number']?>">
                    <i class="fas fa-phone"></i>
                    Call Today for a Consultation…
                </a>
            </div>
        </div>
    </div>

    <div class="title-global--wrap" style="background-color: hsl(39, 87%, 9%)">
        <div class="container">
            <div class="title-global">
                <h2 class="title-font" style="background-color: hsl(39, 87%, 9%)">Integrity. Experience. Results.</h2>
            </div>
        </div>
    </div>

    <div class="contacts--wrap">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="contacts">
                        <a href="<?php the_permalink('785')?>" target="_blank" class="contacts__title title-font">
                            Rutherfordton
                        </a>
                        <p class="contacts__text">
                            187 North Washington Street
                        </p>
                        <p class="contacts__text">
                            Rutherfordton, NC 28139
                        </p>
                        <p class="contacts__text">
                            Hours: Mon-Fr 9 AM - 5AM
                        </p>
                        <a href="https://www.google.com/maps/dir//Farmer+%26+Morris+Law,+187+N+Washington+St,+Rutherfordton,+NC+28139,+United+States/@35.369639,-81.9607107,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x885743e3fbd76067:0xf3594589b553dc42!2m2!1d-81.958522!2d35.369639" target="_blank" class="contacts__link">Get Directions</a>
                        <p class="contacts__phone">
                            Phone:
                            <a href="tel:+1(828)-759-5709">(828)-759-5709</a>
                        </p>
                        <div class="contacts__map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d52056.1057925991!2d-81.953801!3d35.367889!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf3594589b553dc42!2sFarmer%20%26%20Morris%20Law!5e0!3m2!1sen!2sua!4v1596705033253!5m2!1sen!2sua" width="600" height="450" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="contacts">
                        <a href="<?php the_permalink('789')?>" target="_blank" class="contacts__title title-font">
                            Shelby
                        </a>
                        <p class="contacts__text">
                            505 South Lafayette Street
                        </p>
                        <p class="contacts__text">
                            Shelby, NC 28150
                        </p>
                        <p class="contacts__text">
                            Hours: Mon-Fr 9 AM - 5AM
                        </p>
                        <a href="https://www.google.com/maps/dir//Farmer+%26+Morris+Law,+505+S+Lafayette+St,+Shelby,+NC+28150,+United+States/@35.28597,-81.540372,15z/data=!4m8!4m7!1m0!1m5!1m1!1s0x8857203ef0b9b01d:0x203a626e72257b57!2m2!1d-81.540372!2d35.28597" target="_blank" class="contacts__link">Get Directions</a>
                        <p class="contacts__phone">
                            Phone:
                            <a href="tel:+1(704)-387-3428">(704)-387-3428</a>
                        </p>
                        <div class="contacts__map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d52108.88271625307!2d-81.540372!3d35.28597!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x203a626e72257b57!2sFarmer%20%26%20Morris%20Law!5e0!3m2!1sen!2sua!4v1596705522894!5m2!1sen!2sua" width="600" height="450" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="contacts">
                        <a href="<?php the_permalink('787')?>" target="_blank" class="contacts__title title-font">
                            Morganton
                        </a>
                        <p class="contacts__text">
                            408A E Union Street
                        </p>
                        <p class="contacts__text">
                            Morganton, NC 28655
                        </p>
                        <p class="contacts__text">
                            Hours: Mon-Fr 9 AM - 5AM
                        </p>
                        <a href="https://www.google.com/maps/dir//Farmer+%26+Morris+Law,+408+E+Union+St+A,+Morganton,+NC+28655,+United+States/@35.7483646,-81.6871138,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x8850d1d0b74f663b:0x34279df0f9d3fd24!2m2!1d-81.6849251!2d35.7483646?hl=en" target="_blank" class="contacts__link">Get Directions</a>
                        <p class="contacts__phone">
                            Phone:
                            <a href="tel:+1(828)-237-6005">(828)-237-6005</a>
                        </p>
                        <div class="contacts__map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12952.397037573895!2d-81.684925!3d35.748365!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x34279df0f9d3fd24!2sFarmer%20%26%20Morris%20Law!5e0!3m2!1sen!2sua!4v1596705750332!5m2!1sen!2sua" width="600" height="450" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="contacts">
                        <a href="<?php the_permalink('791')?>" target="_blank" class="contacts__title title-font">
                            Spartanburg
                        </a>
                        <p class="contacts__text">
                            220 North Church Street, Suite 2
                        </p>
                        <p class="contacts__text">
                            Spartanburg, SC 29306
                        </p>
                        <p class="contacts__text">
                            Hours: Mon-Fr 9 AM - 5AM
                        </p>
                        <a href="https://www.google.com/maps/dir//Farmer+%26+Morris+Law,+220+N+Church+St+%232,+Spartanburg,+SC+29306,+United+States/@34.952099,-81.9358337,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x885775f49cf80193:0x17b1183341dc8746!2m2!1d-81.933645!2d34.952099?hl=en" target="_blank" class="contacts__link">Get Directions</a>
                        <p class="contacts__phone">
                            Phone:
                            <a href="tel:+1(864)-383-0234">(864)-383-0234</a>
                        </p>
                        <div class="contacts__map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13080.71966989759!2d-81.933645!3d34.952099!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x17b1183341dc8746!2sFarmer%20%26%20Morris%20Law!5e0!3m2!1sen!2sua!4v1596705887798!5m2!1sen!2sua" width="600" height="450" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php }

genesis();

