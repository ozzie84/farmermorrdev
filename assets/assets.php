<?php

add_action( 'wp_enqueue_scripts', 'custom_include_assets' );

function custom_include_assets() {
    $jsArray = [
      CHILD_URL . '/assets/libs/slideout/slideout.min.js',
      CHILD_URL . '/assets/libs/page-scroll-to-id/jquery.malihu.PageScroll2id.js'
//        CHILD_URL . '/assets/libs/izimodal/js/iziModal.msin.js',
//        CHILD_URL . '/assets/libs/swiper/js/swiper.min.js',
    ];

    $cssArray = [
//        CHILD_URL . '/assets/libs/izimodal/css/iziModal.min.css',
//        CHILD_URL . '/assets/libs/swiper/css/swiper.min.css'
    ];

    foreach($cssArray as $css_asset) {
        wp_enqueue_style( basename($css_asset), $css_asset );
    }

    foreach($jsArray as $js_asset) {
        wp_enqueue_script( basename($js_asset), $js_asset, array('jquery'), '1.0.0', true );
    }


    // include general theme files
    wp_enqueue_style( 'theme-main', CHILD_URL . '/assets/app/css/main.css' );
    wp_enqueue_script( 'theme-main', CHILD_URL . '/assets/app/js/main.js', array('jquery'), '1.0.0', true );

}

// Remove attr in style|scripts
add_action(
    'after_setup_theme',
    function() {
        add_theme_support( 'html5', [ 'script', 'style' ] );
    }
);