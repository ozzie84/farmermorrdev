"use strict";

(function ($) {
  // FAQ Sorting Functionality
  $('#filters a').click(function (e) {
    e.preventDefault();
    var term = $(this).data('filter');
    $(this).addClass('filter-active').siblings().removeClass('filter-active');

    if (term === 'all') {
      $('#items > div').fadeIn();
    } else {
      $('#items > div').fadeOut(0);
      $('#items > div[data-filter-item="' + term + '"]').fadeIn();

      if ($('#items > div[data-filter-item="' + term + '"]').length == 2) {
        $('#items').css('justify-content', 'space-around');
      } else {
        $('#items').css('justify-content', 'space-between');
      }
    }
  });
  /* Activate Mobile menu*/
  //Set the main site container

  var siteContainer = document.getElementsByClassName('main-wrap');
  var menuContainer = document.getElementsByClassName('mobile-menu');
  var slideout = new Slideout({
    'panel': siteContainer[0],
    'menu': menuContainer[0],
    'padding': 256,
    'tolerance': 70,
    'easing': 'ease',
    'side': 'right',
    'touch': false
  });
  /* Workarrounds for a fixed menu */
  //Add to translate Header, Main site wrap and Footer

  slideout.on('translate', function (translated) {
    //Fixed header animation
    jQuery('.site-header').css('transition', '-webkit-transform 0ms ease');
    jQuery('.site-header').css('-webkit-transform', 'translateX(' + translated + 'px)');
    jQuery('.site-header').css('transform', 'translateX(' + translated + 'px)'); //Site inner animation

    jQuery('.main-wrap').css('transition', '-webkit-transform 0ms ease');
    jQuery('.main-wrap').css('-webkit-transform', 'translateX(' + translated + 'px)');
    jQuery('.main-wrap').css('transform', 'translateX(' + translated + 'px)'); //Footer animation

    jQuery('.site-footer').css('transition', '-webkit-transform 0ms ease');
    jQuery('.site-footer').css('-webkit-transform', 'translateX(' + translated + 'px)');
    jQuery('.site-footer').css('transform', 'translateX(' + translated + 'px)'); //Page H1 animation

    jQuery('.homepage-hero-image').css('transition', '-webkit-transform 0ms ease');
    jQuery('.homepage-hero-image').css('-webkit-transform', 'translateX(' + translated + 'px)');
    jQuery('.homepage-hero-image').css('transform', 'translateX(' + translated + 'px)'); //Inner Page H1 animation

    jQuery('.internal-hero-image').css('transition', '-webkit-transform 0ms ease');
    jQuery('.internal-hero-image').css('-webkit-transform', 'translateX(' + translated + 'px)');
    jQuery('.internal-hero-image').css('transform', 'translateX(' + translated + 'px)'); //Copyright animation

    jQuery('.copyright').css('transition', '-webkit-transform 0ms ease');
    jQuery('.copyright').css('-webkit-transform', 'translateX(' + translated + 'px)');
    jQuery('.copyright').css('transform', 'translateX(' + translated + 'px)');
  });
  slideout.on('beforeclose', function () {
    //Fixed header animation
    jQuery('.site-header').css('transition', '-webkit-transform 300ms ease');
    jQuery('.site-header').css('-webkit-transform', 'translateX(0px)');
    jQuery('.site-header').css('transform', 'translateX(0px)'); //Site inner animation

    jQuery('.main-wrap').css('transition', '-webkit-transform 300ms ease');
    jQuery('.main-wrap').css('-webkit-transform', 'translateX(0px)');
    jQuery('.main-wrap').css('transform', 'translateX(0px)'); //Footer animation

    jQuery('.site-footer').css('transition', '-webkit-transform 300ms ease');
    jQuery('.site-footer').css('-webkit-transform', 'translateX(0px)');
    jQuery('.site-footer').css('transform', 'translateX(0px)'); //Page H1 animation

    jQuery('.homepage-hero-image').css('transition', '-webkit-transform 300ms ease');
    jQuery('.homepage-hero-image').css('-webkit-transform', 'translateX(0px)');
    jQuery('.homepage-hero-image').css('transform', 'translateX(0px)'); //Inner Page H1 animation

    jQuery('.internal-hero-image').css('transition', '-webkit-transform 300ms ease');
    jQuery('.internal-hero-image').css('-webkit-transform', 'translateX(0px)');
    jQuery('.internal-hero-image').css('transform', 'translateX(0px)'); //Copyright animation

    jQuery('.copyright').css('transition', '-webkit-transform 300ms ease');
    jQuery('.copyright').css('-webkit-transform', 'translateX(0px)');
    jQuery('.copyright').css('transform', 'translateX(0px)');
  });
  slideout.on('beforeopen', function () {
    //Fixed header animation
    jQuery('.site-header').css('transition', '-webkit-transform 300ms ease');
    jQuery('.site-header').css('-webkit-transform', 'translateX(-255px)');
    jQuery('.site-header').css('transform', 'translateX(-255px)'); //Site inner animation

    jQuery('.main-wrap').css('transition', '-webkit-transform 300ms ease');
    jQuery('.main-wrap').css('-webkit-transform', 'translateX(-255px)');
    jQuery('.main-wrap').css('transform', 'translateX(-255px)'); //Footer animation

    jQuery('.site-footer').css('transition', '-webkit-transform 300ms ease');
    jQuery('.site-footer').css('-webkit-transform', 'translateX(-255px)');
    jQuery('.site-footer').css('transform', 'translateX(-255px)'); //Page H1 animation

    jQuery('.homepage-hero-image').css('transition', '-webkit-transform 300ms ease');
    jQuery('.homepage-hero-image').css('-webkit-transform', 'translateX(-255px)');
    jQuery('.homepage-hero-image').css('transform', 'translateX(-255px)'); //Inner Page H1 animation

    jQuery('.internal-hero-image').css('transition', '-webkit-transform 300ms ease');
    jQuery('.internal-hero-image').css('-webkit-transform', 'translateX(-255px)');
    jQuery('.internal-hero-image').css('transform', 'translateX(-255px)'); //Copyright animation

    jQuery('.copyright').css('transition', '-webkit-transform 300ms ease');
    jQuery('.copyright').css('-webkit-transform', 'translateX(-255px)');
    jQuery('.copyright').css('transform', 'translateX(-255px)');
  }); // TODO Page scroll to id init

  $("a[href*='#']").mPageScroll2id({
    autoScrollSpeed: true
  }); // TODO Burger

  $('button.menu-button').on('click', function (event) {
    event.preventDefault();
    $(this).toggleClass('menu-active');
    $('.header__menu').toggleClass('header__menu--active');
    $('body').toggleClass('overflow-global');
    $('.search-custom').fadeOut(300);
    $('.search-custom input').val('');
  }); // TODO Search

  $('.btn-search').on('click', function (event) {
    event.preventDefault();
    $('.search-custom').fadeIn(300);
    $('.search-custom input').focus();
    $('button.menu-button').removeClass('menu-active');
    $('.header__menu').removeClass('header__menu--active');
    $('body').removeClass('overflow-global');
  });
  $('.search-custom').on('click', function (event) {
    event.preventDefault();
    $('.search-custom').fadeOut(300);
    $('.search-custom input').val('');
  }); // TODO Button up

  function windowScroll() {
    var btn = document.querySelector('.scroll-btn');

    if (this.scrollY > 54) {
      $('.header__top').hide();
      $('.site-container').addClass('fix-header');
      $('.header--wrap').addClass('header__sticky');
    } else {
      $('.header__top').show();
      $('.site-container').removeClass('fix-header');
      $('.header--wrap').removeClass('header__sticky');
    }

    if (this.scrollY > 500) {
      btn.classList.add('sticky-btn');
    } else {
      btn.classList.remove('sticky-btn');
    }
  }

  window.addEventListener("scroll", windowScroll, false); // TODO Accordion

  $('.asked__btn').on('click', function (event) {
    event.preventDefault();
    $(this).toggleClass('asked--active').next().slideToggle(100);
  }); // TODO Contact Us Events

  $('.contact_us').on('click', function (event) {
    event.preventDefault();
    $('.form-contacts--wrap').addClass('form-contacts--active');
    $('.overlay').fadeIn(300);
    $('body').addClass('overflow-global');
  });
  $('.overlay, .form-contacts__close').on('click', function (event) {
    event.preventDefault();
    $('.form-contacts--wrap').removeClass('form-contacts--active');
    $('.overlay').fadeOut(300);
    $('body').removeClass('overflow-global');
  }); // TODO Counter on scroll
  // if ($('body').hasClass('home')) {
  //     let aNum = 0;
  //     $(window).scroll(function() {
  //         let oTop = $('#counterJS').offset().top - window.innerHeight;
  //         if (aNum == 0 && $(window).scrollTop() > oTop) {
  //             $('.data__sub').each(function() {
  //                 let $this = $(this),
  //                     countTo = $this.attr('data-count');
  //                 $({
  //                     countNum: $this.text()
  //                 }).animate({
  //                         countNum: countTo
  //                     },
  //                     {
  //                         duration: 2500,
  //                         easing: 'swing',
  //                         step: function() {
  //                             $this.text(Math.floor(this.countNum));
  //                         },
  //                         complete: function() {
  //                             $this.text(this.countNum);
  //                         }
  //                     });
  //             });
  //             aNum = 1;
  //         }
  //     });
  // }
  // Redirect Thanks Page
  // document.addEventListener( 'wpcf7mailsent', function( event ) {
  //     window.location.href = '/thank-you-2/';
  // }, false );
})(jQuery);