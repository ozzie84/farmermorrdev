<?php

global $hc_settings;

$hc_settings = [
    'location_taxonomy' => 'hc_city_location',
    'location_widget_title' => 'city_location_widget_title',
    'faqs_category_taxonomy' => 'hc_faqs',
    'state' => 'State Name',
    'stateabbr' => 'ST',
    'phone_number' => '(828)-759-5709',
    'primary_menu' => "Primary Menu",
    'practice_areas_menu_item' => "menu-item-00",
];


// generate widget title field
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array (
        'key' => 'group_5dcda98072a23',
        'title' => 'City Widget Title',
        'fields' => array (
            array (
                'key' => 'field_5dcda98c6d1dd',
                'label' => 'Location Widget Title',
                'name' => $hc_settings['location_widget_title'],
                'type' => 'text',
                'value' => NULL,
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
endif;





if(is_admin()):
// setup plugins
require_once 'plugin-activator/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 *  <snip />
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function my_theme_register_required_plugins() {

    $plugins = array(

        array(
            'name'      => 'Custom Permalinks',
            'slug'      => 'custom-permalinks',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Yoast SEO',
            'slug'      => 'wordpress-seo',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Redirection',
            'slug'      => 'redirection',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Custom Fields',
            'slug'      => 'advanced-custom-fields',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Simply Show Hooks',
            'slug'      => 'simply-show-hooks',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'reSmush.it',
            'slug'      => 'resmushit-image-optimizer',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Autoptimize',
            'slug'      => 'autoptimize',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Classic Editor',
            'slug'      => 'classic-editor',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Accessibility by UserWay',
            'slug'      => 'userway-accessibility-widget',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'CallTrackingMetrics',
            'slug'      => 'call-tracking-metrics',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'WP External Links',
            'slug'      => 'wp-external-links',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),
        
        array(
            'name'      => 'Contact Form 7 Database Addon – CFDB7',
            'slug'      => 'contact-form-cfdb7',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),
        
        array(
            'name'      => 'WebP Express',
            'slug'      => 'webp-express',
            'required'  => false,
            'force_activation' => false,
            'force_deactivation' => false
        ),

    );

    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php',            // Parent menu slug.
        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => true,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        /*
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'theme-slug' ),
            'menu_title'                      => __( 'Install Plugins', 'theme-slug' ),
            // <snip>...</snip>
            'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
        */
    );

    tgmpa( $plugins, $config );

}
endif;



/*
 *
 * AUTO INCLUDE - BE CAREFUL!
 *
 * */

$autoiclude_folders = [
    '/lib/snippets/',
    '/lib/taxonomy/',
    '/lib/shortcodes/',
    '/lib/widgets/',
];

foreach($autoiclude_folders as $folder) {
    foreach (scandir(dirname(__FILE__) . $folder) as $filename) {
        $path = dirname(__FILE__) . $folder . $filename;
        if (is_file($path)) {
            require_once $path;
        }
    }
}


// include layout
require_once 'lib/layout/layout.php';

// include frontend assets
require_once 'assets/assets.php';

// allow shortcodes in text widgets
add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode');

// add id main
add_filter('genesis_attr_site-container', 'jive_attributes_st_inner');
function jive_attributes_st_inner($attributes) {
    $attributes['id'] .= 'main-content-custom';
    return $attributes;
}

// Remove 'p' in form
add_filter('wpcf7_autop_or_not', '__return_false');

# удалить атрибут type у scripts и styles
add_filter('style_loader_tag', 'sj_remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'sj_remove_type_attr', 10, 2);
function sj_remove_type_attr($tag) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}