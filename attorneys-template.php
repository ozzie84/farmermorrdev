<?php

/* Template Name: Attorneys template */

// Internal Content

remove_action('genesis_loop', 'genesis_do_loop', 10);
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

add_action('genesis_loop', 'custom_inner_content', 10);
remove_action('genesis_before_content_sidebar_wrap', 'custom_do_breadcrumbs', 5);


function custom_inner_content() {?>

    <div class="main-content">
        <div class="container custom-container">
            <div class="attorneys--wrapper">
                <div class="title-global--wrap" style="background-color: hsl(37, 56%, 62%);">
                    <div class="title-global fs25">
                        <h2 class="title-font" style="background-color: hsl(37, 56%, 62%); color: #2d1e03;">Our Attorneys</h2>
                    </div>
                </div>
                <div class="person--wrap person-xl">
                    <ul class="person">
                        <?php
                        if( have_rows('attorneys_list') ):
                            while( have_rows('attorneys_list') ) : the_row();
                                ?>
                                <li class="person__item"><a class="person__link" href="<?php the_permalink(get_sub_field('page_id'))?>" title="<?php echo get_sub_field('name');?>">
                                        <img src="<?php echo get_sub_field('image');?>" alt="<?php echo get_sub_field('name');?>" /><p><?php echo get_sub_field('name');?></p></a></li>
                            <?php
                            endwhile;
                        endif; ?>
                    </ul>
                </div>
                <div class="title-global--wrap" style="background-color: hsl(37, 56%, 62%);">
                    <div class="title-global fs25">
                        <h2 class="title-font" style="background-color: hsl(37, 56%, 62%); color: #2d1e03;">Integrity . Experience . Results .</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php }

genesis();