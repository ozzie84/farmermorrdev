<?php

function add_asked() {

    ob_start();
    //BEGIN OUTPUT

    ?>

    <div class="section section-asked">
        <div class="title-global--wrap" style="background-color: hsl(37, 56%, 62%)">
            <div class="title-global fs25">
                <h2 class="title-font" style="background-color: hsl(37, 56%, 62%); color: rgb(45,30,3);">Frequently Asked Questions</h2>
            </div>
        </div>
        <div class="asked--wrap">
            <div class="asked">
                <div class="asked__item">
                    <div class="asked__btn">
                        <span class="asked__plus">+</span>
                        <p class="asked__this">How can filing bankruptcy help me?</p>
                    </div>
                    <div class="asked__hidden">

                    </div>
                </div>
                <div class="asked__item">
                    <div class="asked__btn">
                        <span class="asked__plus">+</span>
                        <p class="asked__this">Do I need to file Chapter 7 bankruptcy or Chapter 13 bankruptcy?</p>
                    </div>
                    <div class="asked__hidden">

                    </div>
                </div>
                <div class="asked__item">
                    <div class="asked__btn">
                        <span class="asked__plus">+</span>
                        <p class="asked__this">How long does the bankruptcy process take?</p>
                    </div>
                    <div class="asked__hidden">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('asked', 'add_asked');