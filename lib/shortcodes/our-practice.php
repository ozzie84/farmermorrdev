<?php

function add_our_practice() {

    ob_start();
    //BEGIN OUTPUT

    ?>

    <div class="title-global--wrap" style="background-color: hsl(39, 87%, 9%)">
        <div class="title-global fs25">
            <h2 class="title-font" style="background-color: hsl(39, 87%, 9%); color: #fff;">Our Practice Areas</h2>
        </div>
    </div>
    <div class="layers--wrap">
        <ul class="layers layers--sm">
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-ambulance"></i>
                    </div>
                    <h4 class="layers__title">
                        Personal Injury
                    </h4>
                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('785')?>" class="layers__link">
                        <img src="/wp-content/uploads/project1.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-users"></i>
                    </div>
                    <h4 class="layers__title">
                        Family Law
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('111')?>" class="layers__link">
                        <img src="/wp-content/uploads/project2.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-university"></i>
                    </div>
                    <h4 class="layers__title">
                        Wrongful Death
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('101')?>" class="layers__link">
                        <img src="/wp-content/uploads/project3.jpg" alt="project">
                    </a>
                </div>
            </li>

            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-money-bill-alt"></i>
                    </div>
                    <h4 class="layers__title">
                        Bankruptcy
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('1997')?>" class="layers__link">
                        <img src="/wp-content/uploads/project4.png" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-taxi"></i>
                    </div>
                    <h4 class="layers__title">
                        Speeding Tickets
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('115')?>" class="layers__link">
                        <img src="/wp-content/uploads/project5.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-gavel"></i>
                    </div>
                    <h4 class="layers__title">
                        Criminal Law
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('125')?>" class="layers__link">
                        <img src="/wp-content/uploads/project6.jpg" alt="project">
                    </a>
                </div>
            </li>

            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fab fa-stack-overflow"></i>
                    </div>
                    <h4 class="layers__title">
                        Wills and Estates
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('119')?>" class="layers__link">
                        <img src="/wp-content/uploads/project7.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-balance-scale"></i>
                    </div>
                    <h4 class="layers__title">
                        Civil Litigation
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('117')?>" class="layers__link">
                        <img src="/wp-content/uploads/project8.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-medkit"></i>
                    </div>
                    <h4 class="layers__title">
                        Workers’ Compensation
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('660')?>" class="layers__link">
                        <img src="/wp-content/uploads/project9.jpg" alt="project">
                    </a>
                </div>
            </li>

            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-ambulance"></i>
                    </div>
                    <h4 class="layers__title">
                        Business Law
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('113')?>" class="layers__link">
                        <img src="/wp-content/uploads/project10.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-briefcase"></i>
                    </div>
                    <h4 class="layers__title">
                        Construction Law
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('123')?>" class="layers__link">
                        <img src="/wp-content/uploads/project11.jpg" alt="project">
                    </a>
                </div>
            </li>
            <li class="layers__item">
                <div class="layers__head">
                    <div class="layers__icon">
                        <i class="fas fa-wheelchair"></i>
                    </div>
                    <h4 class="layers__title">
                        Social Security Disability
                    </h4>

                </div>
                <div class="layers__main">
                    <a href="<?php the_permalink('105')?>" class="layers__link">
                        <img src="/wp-content/uploads/project12.jpg" alt="project">
                    </a>
                </div>
            </li>
        </ul>
    </div>

    <?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('our_practice', 'add_our_practice');