<?php

function sidebar_form_shortcode($atts = null) {

    global $hc_settings;

    ob_start();
    //BEGIN OUTPUT

    ?>

    <div class="sidebar-contact-form standard-form">

            <div class="sidebar-contact-form__phone-heading">
                Call <a href="tel:+1<?=$hc_settings['phone_number']?>"><?=$hc_settings['phone_number']?></a>
            </div>

            <div class="sidebar-contact-form__subheading">
                Free Case Evaluation
            </div>

        <?php // add CF7 shortcode and remove those lines: ?>

        <?php echo do_shortcode('[contact-form-7 id="2844" title="form aside" html_class="form-info"]') ?>
            
            
        <?php // end ?>

        <?php //echo do_shortcode( '[contact-form-7 id="" title=""]' ); ?>
    </div>

    <?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}


add_shortcode('sidebar-form', 'sidebar_form_shortcode');