<?php

function title_anime_run() {

    ob_start();
    //BEGIN OUTPUT

    ?>

    <div class="section section-2 text-style-section" style="background-color: hsl(37, 56%, 62%);">
        <div class="container">
            <h3 class="custom-headline no-style">
                <span class="text" id="write" data-writes='["<?php the_field('title-anime');?>", "<?php the_field('title-anime2');?>", "<?php the_field('title-anime3');?>"]'></span>
                <span class="suffix">Contact Us Today</span>
            </h3 >
        </div>
    </div>

    <?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('title_anime', 'title_anime_run');