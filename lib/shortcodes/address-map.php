<?php
/*--------------------------------
Example Shortcode Wrapper
[hc-address
 map="#"
 address1="#"
 address2="#"
 name="#"
 phone="#"]
---------------------------------*/

function hc_address_shortcode($atts = [], $content = null) {
    global $post;

    $address1 = "";
    $address2 = "";
    $phone = "";
    $fax = "";
    $name= "";
    $map = "";
    $additional = "";
    $email = "";
    $link = "";
    $hours = "";
    $anchor_text = "";

    extract( shortcode_atts( array(
        'address1' => '',
        'address2' => '',
        'phone' => '',
        'fax' => '',
        'name' => '',
        'email' => '',
        'map' => '',
        'additional' => '',
        'link' => '',
        'anchor_text' => '',
        'hours' => '',
    ), $atts ) );

    ob_start();
    //BEGIN OUTPUT
    ?>

    <div class="hc-map">
        <div class="hc-map__inner">
            <div class="hc-map__address">
                <h3 class="hc-map__office-title">Office Information</h3>
                <strong>Address</strong>
                <address>
                    <?php echo $name; ?><br>
                    <?php echo $address1; ?>
                    <?php echo $address2; ?><br>

                    <?php echo $additional; ?>
                </address>

                <?php if($hours) {
                    ?>  
                    <strong class="phone-heading"><i class="fa fa-clock-o"></i> </strong>
                    <strong><?php echo $hours; ?></strong>
                    <br>
                    <?php
                } ?>

                <?php if($email) {
                    ?>  
                    <strong class="phone-heading"><i class="fa fa-envelope"></i> </strong>
                    <a class="address__link_" href="mail:<?php echo $email; ?>"><?php echo $email; ?></a>
                    <br>
                    <?php
                } ?>
                <?php if($phone) {
                    ?>
                    <strong class="phone-heading"><i class="fa fa-phone"></i> </strong>
                    <a class="address__link_" href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
                    <br>
                    <?php
                } ?>
                <?php if($fax) {
                    ?>
                    <strong class="phone-heading"><i class="fa fa-fax"></i> </strong>
                    Fax: <a class="address__link_" href="fax:<?php echo $fax; ?>"><?php echo $fax; ?></a>
                    <?php
                } ?>
            </div>
            <div class="hc-map__map-embed">

                <div class="map-container">
                    <iframe src="<?php echo $map; ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

                <div style="text-align: center;margin-top: 10px;">
                    <a href="<?=$link?>" target="_blank"><?=$anchor_text?></a>
                </div>

            </div>
        </div>
    </div>


    <?php
    //END OUTPUT
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}
add_shortcode('hc-address', 'hc_address_shortcode');