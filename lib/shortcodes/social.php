<?php

function add_social() {

    ob_start();
    //BEGIN OUTPUT

    ?>

    <ul class="social">
        <li class="social__item">
            <a href="https://www.facebook.com/farmermorrislaw/" target="_blank" rel="nofollow" class="social__link">
                <i class="fab fa-facebook-square"></i>
            </a>
        </li>
        <li class="social__item">
            <a href="https://www.google.com/maps/place/Farmer+%26+Morris+Law/@35.369609,-81.9583417,17z/data=!4m6!1m3!3m2!1s0x885743e3fbd76067:0xf3594589b553dc42!2sFarmer+%26+Morris+Law!3m1!1s0x885743e3fbd76067:0xf3594589b553dc42" target="_blank" rel="nofollow" class="social__link">
                <i class="fab fa-google-plus-square"></i>
            </a>
        </li>
        <li class="social__item">
            <a href="https://www.linkedin.com/company/tomblin-farmer-&-morris-pllc" target="_blank" rel="nofollow" class="social__link">
                <i class="fab fa-linkedin"></i>
            </a>
        </li>
    </ul>

    <?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}

add_shortcode('social', 'add_social');