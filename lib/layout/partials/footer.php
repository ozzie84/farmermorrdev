<?php

function custom_footer(){

    global $hc_settings;

    remove_action('genesis_footer', 'genesis_do_footer', 10); ?>

    <div class="footer">
        <div class="other-links">
            <a href="<?php the_permalink(2524) ?>" class="sitemap_link">Sitemap</a>
            <a href="<?php the_permalink(253) ?>" class="sitemap_link">Disclaimer</a>
        </div>
        <div class="footer__nav">
            <ul class="menu">
                <li class="menu__item">
                    <a href="/" class="menu__link">Home</a>
                </li>
                <li class="menu__item">
                    <a href="<?php the_permalink(349)?>" class="menu__link">Our Firm</a>
                </li>
                <li class="menu__item">
                    <a href="<?php the_permalink(1210)?>" class="menu__link">Attorneys</a>
                </li>
                <li class="menu__item">
                    <a href="<?php the_permalink(135)?>" class="menu__link">Practice</a>
                </li>
                <li class="menu__item">
                    <a href="<?php the_permalink(143)?>" class="menu__link">Locations</a>
                </li>
                <li class="menu__item">
                    <a href="<?php the_permalink(34)?>" class="menu__link">Contact</a>
                </li>
            </ul>
        </div>
        <div class="footer__social">
            <ul class="social">
                <li class="social__item">
                    <a href="https://www.facebook.com/farmermorrislaw/" target="_blank" rel="nofollow" class="social__link">
                        <i class="fab fa-facebook-square"></i>
                    </a>
                </li>
                <li class="social__item">
                    <a href="https://www.linkedin.com/company/tomblin-farmer-&-morris-pllc" target="_blank" rel="nofollow" class="social__link">
                        <i class="fab fa-linkedin"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="footer__review">
            <p>Farmer & Morris Law, PLLC.has a
                <a href="https://www.google.com/search?q=Farmer+%26+Morris+Law,+187+N+Washington+St,+Rutherfordton,+NC+28139&ludocid=17535123081898744898#lrd=0x885743e3fbd76067:0xf3594589b553dc42,1" target="_blank" rel="nofollow">Google Reviews </a>
                Rating of 4.9/5 based on 118 reviews</p>
        </div>
    </div>

    <a href="#main-content-custom" class="scroll-btn">
        <i class="fas fa-chevron-up"></i>
    </a>

    <div class="search-custom">
        <div class="search-form--wrap">
            <form class="search-form">
                <label for="search" class="search-form__label">
                    Type and Press “enter” to Search
                </label>
                <input type="search" id="search" name="s" autocomplete="off" class="search-form__input">
            </form>
        </div>
    </div>

    <div class="overlay"></div>

    <a href="#" class="contact_us">Contact Us</a>

    <div class="form-contacts--wrap">
        <div class="form-contacts">
            <a href="#" class="form-contacts__close">
                <i class="fas fa-times"></i>
            </a>
            <div class="form-contacts__link">
                Call <a href="tel:+1<?=$hc_settings['phone_number']?>"><?=$hc_settings['phone_number']?></a>
            </div>
            <?php echo do_shortcode('[contact-form-7 id="2847" title="form contact us" html_class="form-this"]')?>
        </div>
    </div>

<?php }

add_action('genesis_footer', 'custom_footer', 8);

function custom_copyright(){ ?>
    <div class="copyright">&copy; <?=date('Y')?> Farmer & Morris | Attorneys at Law</div>
<?php }

add_action('genesis_footer', 'custom_copyright', 20);

//Place mobile menu
function footer_mobile_menu()
{
	$html = '<div class="mobile-menu">';
	$html .= wp_nav_menu([
		'theme_location' => 'primary',
		'container' => '',
		'echo' => false
	]);
	$html .= '</div>';

	return $html;
}

//Adding mobile menu outside the main wrap
add_action('wp_footer', function(){
	echo footer_mobile_menu();
});
