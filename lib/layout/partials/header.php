<?php

add_action('genesis_header', 'custom_do_header', 1);

function custom_do_header(){

    remove_action('genesis_header', 'genesis_header_markup_open', 5);
    remove_action('genesis_header', 'genesis_do_header', 10);
    remove_action('genesis_header', 'genesis_header_markup_close', 15);
    remove_action('genesis_header', 'custom_remove_nav', 12);
    remove_action('genesis_after_header', 'genesis_do_nav', 10);
    remove_action('genesis_after_header', 'genesis_do_subnav', 10);

    global $hc_settings;
    ?>

    <header class="header--wrap">
        <div class="header custom-header">
            <div class="header__top">
                <div class="header__top--fix">
                    <div class="header__phone">
                        <p>
                            P:
                            <a href="tel:+1<?=$hc_settings['phone_number']?>"><?=$hc_settings['phone_number']?></a>
                        </p>
                    </div>
                    <div class="header__social">
                    <ul class="social">
                        <li class="social__item">
                            <a href="https://www.facebook.com/farmermorrislaw/" target="_blank" rel="nofollow" class="social__link">
                                <i class="fab fa-facebook-square"></i>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="https://www.linkedin.com/company/tomblin-farmer-&-morris-pllc" target="_blank" rel="nofollow" class="social__link">
                                <i class="fab fa-linkedin"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                </div>
            </div>
            <div class="header__low">
                <a href="tel:+18287595709" class="phone_mob">
                    <i class="fas fa-phone-alt"></i>
                </a>
                <a href="/" class="logotype">
                    <img src="<?=CHILD_URL?>/assets/dev/img/logo.png" alt="logo">
                </a>
                <div class="header__menu">
                    <?php
                    wp_nav_menu([
                        'menu' =>  'Primary Menu',
                        'container_class'=> 'genesis-nav-menu'
                    ])
                    ?>
                    <div class="header__fix">
                        <div class="header__social">
                            <ul class="social">
                                <li class="social__item">
                                    <a href="https://www.facebook.com/farmermorrislaw/" target="_blank" rel="nofollow" class="social__link">
                                        <i class="fab fa-facebook-square"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="https://www.linkedin.com/company/tomblin-farmer-&-morris-pllc" target="_blank" rel="nofollow" class="social__link">
                                        <i class="fab fa-linkedin"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <button type="button" name="search" class="btn-search">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
                <div class="menu--wrapper">
                    <button type="button" name="search" class="btn-search">
                        <i class="fas fa-search"></i>
                    </button>
                    <button class="menu-button">
                        <i class="fas fa-bars"></i>
                    </button>
                </div>
            </div>
        </div>
    </header>

    <?php
}