<?php

// breadcrumbs
if ( function_exists('yoast_breadcrumb') ) {
    function custom_do_breadcrumbs() {
        remove_action('genesis_before_loop', 'genesis_do_breadcrumbs', 10);

        yoast_breadcrumb( '<p class="breadcrumb" id="breadcrumbs">','</p>' );
    }
    add_action('genesis_before_content_sidebar_wrap', 'custom_do_breadcrumbs', 5);
}


add_action('genesis_before_content_sidebar_wrap', function(){

    global $post;
    global $hc_settings;

    if(is_archive()) {

        $post_cats = wp_get_post_categories($post->ID);

        if($post_cats) {
            $category = get_category(current($post_cats));

            $p = get_posts([
                'post_type'=>'page',
                'meta_key'=>$hc_settings['location_widget_title'],
                'meta_value'=>$category->name,
                'tax_query' => [
                    [
                        'taxonomy'=> $hc_settings['location_taxonomy'],
                        'terms' => [0] // insert tag id of your main location city of location taxonomy
                    ]
                ],
            ]);

            if($p) {
                $p = current($p);
            } else {
                return '';
            }

            ?>
            <div class="container-fluid container-custom container-practice-link">
                <a href="<?=get_permalink($p->ID)?>" title="<?=get_the_title($p->ID)?>"><?=get_the_title($p->ID)?></a>
            </div>
            <?php
        }

    }

}, 10);

//add proper breadcrumbs for location pages
function filter_wpseo_breadcrumb_links($crumbs) {
    global $post, $hc_settings;

    $terms = get_the_terms($post->ID, $hc_settings['faqs_category_taxonomy']);

    if(is_singular('page') && $terms) {

    	$term = current($terms);

        $add_crumbs = [
            [
                'text' => $term->name,
                'url' => get_term_link($term, $hc_settings['faqs_category_taxonomy'])
            ]
        ];


        array_splice($crumbs, -1, 0, $add_crumbs);
    }

    if(is_tax($hc_settings['faqs_category_taxonomy'])) {

        $add_crumbs = [
            [
                'text' => 'Frequently Asked Questions',
                'url' => '/faqs/'
            ]
        ];


        array_splice($crumbs, -1, 0, $add_crumbs);
    }

    return $crumbs;
}
add_filter( 'wpseo_breadcrumb_links', 'filter_wpseo_breadcrumb_links', 10, 1 );


add_filter( 'wpseo_breadcrumb_links', 'hd_change_location_breadcrumbs' );
function hd_change_location_breadcrumbs( $links ) {
	global $hc_settings;
   	foreach($links as $k => $link) {
		if($link['id']) {
			$page = get_post($link['id']);
			if($widget_title = get_field($hc_settings['location_widget_title'], $link['id'])) {

				$city = get_the_terms($link['id'], $hc_settings['location_taxonomy']) ? current(get_the_terms($link['id'], $hc_settings['location_taxonomy'])) : false;

				if($city) {
					if($widget_title == 'Personal Injury') {
// leave same as h1
					}

					else if($widget_title && $widget_title !== '') {
						$links[$k] = [
							'url' => get_permalink($link['id']),
							'text' => $widget_title
						];
					}
				}

			}

		}
   	}

    return $links;
}