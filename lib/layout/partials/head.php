<?php

// Head Scripts and Styles (Google Analytics etc)
add_action('wp_head', 'custom_head_scripts_and_styles');

function custom_head_scripts_and_styles(){ ?>

    <!-- Google Analytics -->
    <script>
        window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
        ga('create', 'GTM-TFT92SL', 'auto');
        ga('send', 'pageview');
    </script>
    <script async src='https://www.google-analytics.com/analytics.js'></script>
    <!-- End Google Analytics -->

<!--    <script>(function(d){var s = d.createElement("script");s.setAttribute("data-account", "w4eQzGNLiC");s.setAttribute("src", "https://cdn.userway.org/widget.js");(d.body || d.head).appendChild(s);})(document)</script>-->
    <script defer src="https://kit.fontawesome.com/8e85bd567e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=PT+Serif+Caption&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
<!--    <script async="" type="text/javascript" src="https://prod.purechatcdn.com/assets/legacy.12013.js"></script>-->
    <?php
}

add_action('genesis_before', 'add_gtag_noscript');

function add_gtag_noscript() {
    ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TFT92SL"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php
}

