<?php

/* Template Name: Out Firm template */

// Internal Content

remove_action('genesis_loop', 'genesis_do_loop', 10);
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

add_action('genesis_loop', 'custom_inner_content', 10);
remove_action('genesis_before_content_sidebar_wrap', 'custom_do_breadcrumbs', 5);


function custom_inner_content() {?>

    <div class="main-content">
        <div class="container custom-container">
            <div class="section section-1 our-firm-intro">
                <div class="container no-padding">
                    <div class="our-firm-intro-content">
                        <p class="intro-content"><?php the_field('about-firm-text');?></p>
                    </div>
                    <div class="our-firm-intro-img"><img class="intro-img" src="<?php the_field('about-firm-image');?>" alt="Law Offices of Farmer &amp; Morris, PLLC Attorneys" /></div>
                </div>
            </div>
            <div class="our-firm-staff">
                <div class="our-staff-heading">
                    <div class="title-global--wrap" style="background-color: hsl(37, 56%, 62%);">
                        <div class="title-global fs25">
                            <h2 class="title-font" style="background-color: hsl(37, 56%, 62%); color: #2d1e03;">Farmer &amp; Morris Law, PLLC Staff</h2>
                        </div>
                    </div>
                </div>
                <div class="our-staff-info">
                    <div class="row staff-list">
                        <?php
                        if( have_rows('staff-list') ):
                            while( have_rows('staff-list') ) : the_row();
                                ?>
                                <div class="col-lg-4 col-md-6 col-xs-12 our-staff-column">
                                    <div class="staff-column">
                                        <div class="staff-info-contents">
                                            <div class="staff-info-image"><img class="staff-img" src="<?php echo get_sub_field('image');?>" alt="<?php echo get_sub_field('name');?>" /></div>
                                            <div class="staff-info-content">
                                                <h3 class="staff-name no-margin"><?php echo get_sub_field('name');?></h3>
                                                <h4 class="staff-roll no-margin"><?php echo get_sub_field('job');?></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            endwhile;
                        endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php }

genesis();